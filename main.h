/*  =========================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.
    ============================================================================
    Revision Information:
        File name: main.h
        Version:   v1.0
        Date:      04-04-2016
    =========================================================================
*/


/*
** ==========================================================================
**                        INCLUDE STATEMENTS
** ==========================================================================
*/

#ifndef PLANE_H
#define	PLANE_H

#include <p24FJ64GB002.h>
#include <string.h>

#include "spi/spi.h"
#include "nr905/nrf.h"


/*
** =============================================================================
**                       EXPORTED FUNCTION DECLARATION
** =============================================================================
*/


#endif	/* PLANE_H */